// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
     // Initialize Firebase
     apiKey: "AIzaSyAZye9HE5Aaiq6Pe3oGVWU7LVnMEEj2NUU",
     authDomain: "homework4-2e465.firebaseapp.com",
     databaseURL: "https://homework4-2e465.firebaseio.com",
     projectId: "homework4-2e465",
     storageBucket: "homework4-2e465.appspot.com",
     messagingSenderId: "424168281813"
    }
  };

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
