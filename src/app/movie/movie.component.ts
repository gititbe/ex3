import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.css']
})
export class MovieComponent implements OnInit {
  @Input() data:any;

  id;
  title;
  Studio;
  Weekend_income;
  show = true;

  constructor() { }

  ngOnInit() {

    this.id = this.data.id;
    this.title = this.data.title;
    this.Studio = this.data.Studio;
    this.Weekend_income = this.data.weekend_income;
  }
  removeMovie(){
    this.show = false;
  }
  getIsShow(){
    return false;
    // return this.show;
  }
}
